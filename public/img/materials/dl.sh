#!/bin/bash

if [ ${1: -4} == '.png' ]
then
  wget $1 -O "$2.png"
else
  wget $2 -O "$1.png"
fi
