import React, { useReducer, useContext } from 'react'
import _ from 'lodash'

const UserContext = React.createContext()

const reducer = (user, action) => {
	const _user = { ...user }

	let _mats

	const medalsReducer = (acc, m) => {
		const remaining = Math.max(m.need - _user.materials[m.id], 0)
		return acc + Math.ceil(remaining / 10) * m.cost
	}

	switch (action.type) {
		case 'HERO_CHANGED':
			_user.selectedHero = action.hero
			break

		case 'MATERIAL_CHANGED':
			_user.materials[action.material] = action.count
			for (const hero in _user.heroes) {
				_mats = _user.heroes[hero].materials
				_user.heroes[hero].medals = _.values(_mats).reduce(medalsReducer, 0)
			}

			break

		case 'MASTERY_CHANGED':
			_user.heroes[action.hero].masteries[action.branch] = action.mastery
			break

		case 'MATERIALS_ADD':
			_mats = _user.heroes[action.hero].materials
			for (const m of action.materials) {
				_mats[m.name].need += m.need
			}

			_user.heroes[action.hero].medals = _.values(_mats).reduce(
				medalsReducer,
				0,
			)

			break

		case 'MATERIALS_REMOVE':
			_mats = _user.heroes[action.hero].materials
			for (const m of action.materials) {
				_mats[m.name].need -= m.need
			}

			_user.heroes[action.hero].medals = _.values(_mats).reduce(
				medalsReducer,
				0,
			)

			break

		default:
			throw new Error('Unexpected action')
	}

	localStorage.setItem('user', JSON.stringify(_user))
	return _user
}

const UserProvider = ({ children }) => {
	const contextValue = useReducer(
		reducer,
		JSON.parse(localStorage.getItem('user')) || require('../json/user.json'),
	)

	return (
		<UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
	)
}

const useUser = () => {
	const contextValue = useContext(UserContext)
	return contextValue
}

export { UserProvider as default, useUser }
