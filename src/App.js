import React from 'react'
import { Container } from 'semantic-ui-react'

import HeroSelector from './components/HeroSelector'
import UserProvider from './hooks/user'

const App = () => (
	<UserProvider>
		<Container>
			<HeroSelector />
		</Container>
	</UserProvider>
)

export default App
