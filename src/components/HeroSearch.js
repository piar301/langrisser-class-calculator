import React, { useState } from 'react'
import _ from 'lodash'
import { Search } from 'semantic-ui-react'

import heroes from '../json/heroes.json'
import HeroSearchResult from './HeroSearchResult.js'
import { useUser } from '../hooks/user'

const HeroSearch = ({ ...props }) => {
	const [{ heroes: userHeroes }, dispatchUser] = useUser()
	const [loading, setLoading] = useState(false)
	const [results, setResults] = useState([])

	const heroResults = heroes.map(h => ({
		title: h.name,
		image: h.image,
		medals: userHeroes[h.name].medals,
	}))

	heroResults.sort((one, two) => one.title - two.title)

	const select = result => {
		document.activeElement.blur()
		setResults([])
		dispatchUser({ type: 'HERO_CHANGED', hero: result.title })
	}

	const queryChanged = q => {
		setLoading(true)

		setTimeout(() => {
			if (q.length < 1) {
				setLoading(false)
				return
			}

			const re = new RegExp(_.escapeRegExp(q), 'i')
			const isMatch = result => re.test(result.title)

			setLoading(false)
			setResults(_.filter(heroResults, isMatch))
		}, 300)
	}

	return (
		<Search
			loading={loading}
			results={results}
			resultRenderer={({ ...props }) => <HeroSearchResult {...props} />}
			onResultSelect={(_, { result }) => select(result)}
			onSearchChange={_.debounce((_, { value }) => queryChanged(value), 500, {
				leading: true,
			})}
			{...props}
		/>
	)
}

export default HeroSearch
