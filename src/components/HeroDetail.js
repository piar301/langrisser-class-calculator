import React from 'react'
import { Divider } from 'semantic-ui-react'

import ClassTree from './ClassTree'
import Materials from './Materials'

const HeroDetail = () => (
	<>
		<ClassTree />
		<Divider />
		<Materials />
	</>
)

export default HeroDetail
