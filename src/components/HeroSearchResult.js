import React from 'react'
import { Label, Icon, Image, Card } from 'semantic-ui-react'

const HeroSearchResult = ({ title, image, medals }) => {
	let extra

	if (medals > 0) {
		extra = (
			<Label ribbon color="red">
				<Icon name="tags" />
				{medals}
			</Label>
		)
	} else {
		extra = <Icon name="smile" color="green" size="big" />
	}

	return (
		<Card>
			<Card.Content>
				<Image size="tiny" src={image} floated="right" avatar wrapped />
				<Card.Header>
					{title}
					{extra}
				</Card.Header>
				<Card.Meta style={{ marginTop: 5 }}></Card.Meta>
			</Card.Content>
		</Card>
	)
}

export default HeroSearchResult
