import React from 'react'
import { Menu, Container, Label, Image, Responsive } from 'semantic-ui-react'

import HeroSearch from './HeroSearch'
import { useUser } from '../hooks/user'
import allHeroes from '../json/heroes.json'

const TopBar = () => {
	const [{ selectedHero, heroes }] = useUser()
	const userHero = heroes[selectedHero]

	// TODO: [WIPE]: move the image in user.json, delete heroes.json
	console.log(userHero)
	const image = userHero
		? allHeroes.filter(h => h.name === selectedHero)[0].image
		: null

	return (
		<Menu fixed="top" inverted size="tiny">
			<Container>
				<Menu.Menu position="left">
					<Menu.Item>
						<HeroSearch />
					</Menu.Item>
				</Menu.Menu>
				{userHero && (
					<Menu.Menu position="right">
						<Menu.Item>
							<Responsive minWidth={500}>
								<Image
									src={image}
									avatar
									spaced="right"
									style={{ height: 40, width: 40 }}
								/>
							</Responsive>
							{selectedHero}
						</Menu.Item>
						<Menu.Item>
							<Responsive minWidth={381}>
								<Label color="black" image>
									<Image src="/img/guild_medal.png" />
									{userHero.medals}
								</Label>
							</Responsive>
							<Responsive maxWidth={380}>
								<Label color="black">
									<Image centered src="/img/guild_medal.png" />
									{userHero.medals}
								</Label>
							</Responsive>
						</Menu.Item>
					</Menu.Menu>
				)}
			</Container>
		</Menu>
	)
}

export default TopBar
