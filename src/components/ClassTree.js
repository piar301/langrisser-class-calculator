import React from 'react'
import { Grid } from 'semantic-ui-react'

import ClassMastery from './ClassMastery'

const ClassTree = () => {
	return (
		<Grid columns={3} textAlign="center">
			{[0, 1, 2].map(row => (
				<Grid.Row key={row}>
					{[0, 1, 2].map(col => (
						<Grid.Column key={col}>
							<ClassMastery row={row} col={col} />
						</Grid.Column>
					))}
				</Grid.Row>
			))}
		</Grid>
	)
}

export default ClassTree
