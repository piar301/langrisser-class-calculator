import React from 'react'
import { Table, Input, Header, Image, Label, Icon } from 'semantic-ui-react'

import { useUser } from '../hooks/user'

const MaterialsRow = ({ id, need, name, image, cost }) => {
	const [user, dispatchUser] = useUser()
	const have = user.materials[id]
	const statusColor = have >= need ? 'green' : 'red'
	const toBuy = Math.ceil(Math.max(need - have, 0) / 10)

	const onHaveChange = h => {
		dispatchUser({
			type: 'MATERIAL_CHANGED',
			material: id,
			count: Math.min(Math.max(h, 0), 99),
		})
	}

	return (
		<Table.Row>
			<Table.Cell>
				{/* TODO: remove arrows from number input */}
				<Input
					type="number"
					style={{ width: 80 }}
					value={have}
					onChange={(_, { value }) => onHaveChange(value)}
				/>
			</Table.Cell>
			<Table.Cell>
				<Header as="h6" image>
					<Image
						src={image}
						size="big"
						label={{
							color: statusColor,
							floating: true,
							content: `${have}/${need}`,
							size: 'mini',
						}}
					/>
					<br />
					<br />
					<Label basic color={statusColor} size="small" pointing>
						{name}
					</Label>
				</Header>
			</Table.Cell>
			<Table.Cell textAlign="left">
				{toBuy > 0 && (
					<>
						<Label basic size="large">
							<Icon name="shopping basket" size="large" />
							{toBuy}
						</Label>
						<br />
						<br />
						<Label basic size="large">
							<Image spaced="right" src="/img/guild_medal.png" size="mini" />
							{toBuy * cost}
						</Label>
					</>
				)}
			</Table.Cell>
		</Table.Row>
	)
}

export default MaterialsRow
