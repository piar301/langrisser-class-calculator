import React from 'react'
import { Container } from 'semantic-ui-react'

import HeroDetail from './HeroDetail'
import TopBar from './TopBar'
import { useUser } from '../hooks/user'

const HeroSelector = () => {
	const [{ selectedHero }] = useUser()

	return (
		<>
			<TopBar />
			<Container text style={{ marginTop: '7em' }}>
				{selectedHero && <HeroDetail />}
			</Container>
		</>
	)
}

export default HeroSelector
