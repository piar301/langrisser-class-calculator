import React from 'react'
import { Card, Rating, Header } from 'semantic-ui-react'

import { useUser } from '../hooks/user'
import trees from '../json/trees.json'

const ClassMastery = ({ row, col }) => {
	const [{ selectedHero: hero, heroes }, dispatchUser] = useUser()

	const branch = trees[hero][row][col]
	if (branch === null) {
		return <></>
	}

	const userMastery = heroes[hero].masteries[branch.name]
	const isMastered = userMastery === branch.tiers.length

	const onMasteryChange = mastery => {
		if (mastery === userMastery) {
			return
		}

		let diff = []
		const [start, end] = [mastery, userMastery].sort()
		for (let i = start; i < end; i++) {
			diff = [...diff, ...branch.tiers[i].materials]
		}

		dispatchUser({
			type: mastery < userMastery ? 'MATERIALS_ADD' : 'MATERIALS_REMOVE',
			materials: diff,
			hero,
		})

		dispatchUser({
			type: 'MASTERY_CHANGED',
			branch: branch.name,
			mastery,
			hero,
			row,
			col,
		})
	}

	return (
		<Card color={isMastered ? 'green' : 'red'}>
			<Card.Content>
				<Card.Description>
					<Header as="h5">{branch.name}</Header>
					<Rating
						clearable
						icon="heart"
						rating={userMastery}
						maxRating={branch.tiers.length}
						onRate={(_, { rating }) => onMasteryChange(rating)}
					/>
				</Card.Description>
			</Card.Content>
		</Card>
	)
}

export default ClassMastery
