import React from 'react'
import _ from 'lodash'
import { Table } from 'semantic-ui-react'

import { useUser } from '../hooks/user'
import MaterialsRow from './MaterialsRow'
import allMats from '../json/materials.json'

const Materials = () => {
	const [{ selectedHero, heroes }] = useUser()

	const hero = heroes[selectedHero]

	const mats = _.values(hero.materials).filter(m => m.need > 0)
	mats.sort((one, two) => one.order - two.order)

	return (
		<>
			<Table
				unstackable
				striped
				textAlign="center"
				color={hero.medals > 0 ? 'red' : 'green'}
			>
				<Table.Body>
					{mats.map(m => (
						<MaterialsRow key={m.id} {...m} {...allMats[m.id]} />
					))}
				</Table.Body>
			</Table>
		</>
	)
}

export default Materials
